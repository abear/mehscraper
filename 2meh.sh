#!/bin/bash

declare -a USERS=( $(mysql meh -N -s -r -e "select * from users;") )

ITEM=$(python /home/brian/meh/scrape.py)

FROM=$(whoami)

MESSAGE="
Subject: What's on Meh Today: $(date +%F) 
MIME-Version: 1.0 
Content-type: text/html
<html><body>
$(date +%F)
\n<hr>
Alpha v.0.2.0\n
NOTICE: Now using SQL for user management! AND BONUS Google is marking everything as spam... again...\n\n
<hr>
$ITEM
<hr>
Click <a href='http://www.meh.com'>here</a> to go directly to meh.\n
</html></body>
"

for i in ${USERS[@]}; do echo -ne "From: $FROM@$HOSTNAME\nTo: $i\n $MESSAGE" | /usr/sbin/sendmail -t ; done
